﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class Insultos : MonoBehaviour
{

    public static int Option;
    public static bool Selected;
    public static int PuntuacionJugador;
    public static int PuntuacionEnemigo;
    private bool TurnoEnemigo;
    public static bool PrimerTurno;
    private int Respuesta;
    public static int Insulto;
    private bool RespuestaFalsa;

    public AudioClip Bell;
    public AudioClip Punch;

    GameObject ScoreEnemy;
    GameObject ScorePlayer;
    GameObject Option1;
    GameObject Option2;
    GameObject Turno;
    GameObject MainCamera; 

    void Start()
    {
        Option1 = GameObject.Find("BotonOpciones1");
        Option2 = GameObject.Find("BotonOpciones2");
        ScorePlayer = GameObject.Find("ButtonScorePlayer");
        ScoreEnemy = GameObject.Find("ButtonScoreEnemy");
        Turno = GameObject.Find("Turnos");
        MainCamera = GameObject.Find("Main Camera");



        Selected = false;
        PuntuacionEnemigo = 3;
        PuntuacionJugador = 3;
        PrimerTurno = true;
    }

    private void ChangeText()
    {
        if(TurnoEnemigo)
        {
            Turno.GetComponent<UnityEngine.UI.Text>().text = "Turno Enemigo";
            Insulto = Random.Range(1, 17);
            switch (Insulto)
            {
                case 1:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Llevarás mi espada como si fueras un pincho moruno!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Primero deberías dejar de usarla como un plumero."; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "Ah si?";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Tú también!";
                            break;
                    }
                    break;

                case 2:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Luchas como un granjero!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "Qué apropiado, tú peleas como una vaca."; //Correcta
                    Respuesta = 2;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Y tú como un cerdo!";
                            break;
                        case 2:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Claro, como tu eres un profesional...";
                            break;
                    }
                    break;
                case 3:
                    GetComponent<UnityEngine.UI.Text>().text = "¡No hay palabras para describir lo asqueroso que eres!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Sí que las hay, sólo que nunca las has aprendido."; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡No hay palabras para describir tu cara!";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Cara lechuga!";
                            break;
                    }
                    break;
                case 4:
                    GetComponent<UnityEngine.UI.Text>().text = "¡He hablado con simios más educados que tu!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Me alegra que asistieras a tu reunión familiar diaria.";//Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "analfabeto";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "patán";
                            break;
                    }
                    break;
                case 5:
                    GetComponent<UnityEngine.UI.Text>().text = "¡No pienso aguantar tu insolencia aquí sentado!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "Ya te están fastidiando otra vez las almorranas, ¿Eh?"; //Correcta
                    Respuesta = 2;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Pues ven a por mi!";
                            break;
                        case 2:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Levantate si te atreves!";
                            break;
                    }
                    break;
                case 6:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Mi pañuelo limpiará tu sangre!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Ah, Ya has obtenido ese trabajo de barrendero?"; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Un poco de repeto!";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "Muy maduro...";
                            break;
                    }
                    break;
                case 7:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Ha llegado tu HORA, palurdo de ocho patas!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Y yo tengo un SALUDO para ti, Te enteras?"; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡No, TÚ hora a llegado!";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡No te atrevas a tocarme!";
                            break;
                    }
                    break;
                case 8:
                    GetComponent<UnityEngine.UI.Text>().text = "Has dejado ya de usar pañales?";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Por qué? Acaso querías pedir uno prestado?"; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "Y tú?";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "Maldito";
                            break;
                    }
                    break;
                case 9:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Una vez tuve un perro más listo que tu!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Te habrá enseñado todo lo que sabes."; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Tengo un gato más listo que tu";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Tendrá pulgas!";
                            break;
                    }
                    break;
                case 10:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Nadie me ha sacado sangre jamás, y nadie lo hará!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "TAN rápido corres?"; //Correcta
                    Respuesta = 2;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Seré la excepción!";
                            break;
                        case 2:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Que seguridad!";
                            break;
                    }
                    break;
                case 11:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Me das ganas de vomitar!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Me haces pensar que alguien ya lo ha hecho."; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Me das asco!";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Vomita pero no cerca de mi!";
                            break;
                    }
                    break;
                case 12:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Tienes los modales de un mendigo!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Quería asegurarme de que estuvieras a gusto conmigo."; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Y tú?!";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Vete por ahí!";
                            break;
                    }
                    break;
                case 13:
                    GetComponent<UnityEngine.UI.Text>().text = "¡He oído que eres un soplón despreciable!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "Qué pena me da que nadie haya oído hablar de ti"; //Correcta
                    Respuesta = 2;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Tú si que eres un soplón!";
                            break;
                        case 2:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Eso es mentira!";
                            break;
                    }
                    break;
                case 14:
                    GetComponent<UnityEngine.UI.Text>().text = "¡La gente cae a mis pies al verme llegar!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Incluso antes de que huelan tu aliento?"; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Ja!";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Más quisieras!";
                            break;
                    }
                    break;
                case 15:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Demasiado bobo para mi nivel de inteligencia!";
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Estaría acabado si la usases alguna vez."; //Correcta
                    Respuesta = 1;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Demasiado tonto!";
                            break;
                        case 2:
                            Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Bobo?!";
                            break;
                    }
                    break;
                case 16:
                    GetComponent<UnityEngine.UI.Text>().text = "¡Obtuve esta cicatriz en mi cara en una lucha a muerte!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "Espero que ya hayas aprendido a no tocarte la nariz"; //Correcta
                    Respuesta = 2;
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Feo!";
                            break;
                        case 2:
                            Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "Que miedo...";
                            break;
                    }
                    break;
            }
        }
        else
        {
            GetComponent<UnityEngine.UI.Text>().text = "Esperando";
            Turno.GetComponent<UnityEngine.UI.Text>().text = "Turno Jugador";
            Insulto = Random.Range(1, 9);
            switch (Insulto)
            {
                case 1:
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Llevarás mi espada como si fueras un pincho moruno!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Luchas como un granjero!";
                    break;
                case 2:
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Obtuve esta cicatriz en mi cara en una lucha a muerte!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Demasiado bobo para mi nivel de inteligencia!";
                    break;
                case 3:
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Demasiado bobo para mi nivel de inteligencia!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Tienes los modales de un mendigo!";
                    break;
                case 4:
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Luchas como un granjero!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Demasiado bobo para mi nivel de inteligencia!";
                    break;
                case 5:
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Nadie me ha sacado sangre jamás, y nadie lo hará!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Luchas como un granjero!";
                    break;
                case 6:
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Ha llegado tu HORA, palurdo de ocho patas!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Obtuve esta cicatriz en mi cara en una lucha a muerte!";
                    break;
                case 7:
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Llevarás mi espada como si fueras un pincho moruno!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Obtuve esta cicatriz en mi cara en una lucha a muerte!";
                    break;
                case 8:
                    Option1.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡Me das ganas de vomitar!";
                    Option2.GetComponentInChildren<UnityEngine.UI.Text>().text = "¡La gente cae a mis pies al verme llegar!";
                    break;
            }
            Respuesta = Random.Range(1, 3);
            
        }
    }
   
    private void AsignarPrimerTurno()
    {
        MainCamera.GetComponent<AudioSource>().PlayOneShot(Bell);
        switch (Random.Range(1, 3))
        {
            case 1:
                TurnoEnemigo = false;
                break;
            case 2:
                TurnoEnemigo = true;
                break;
        }
    }

    IEnumerator Waiter()
    {
        yield return new WaitForSeconds(3f);
    }

    public void Comprobacion()
    {
        MainCamera.GetComponent<AudioSource>().PlayOneShot(Punch);
        if (TurnoEnemigo)
        {
            if (Respuesta == Option)
            {
                RespuestaFalsa = false;
                PuntuacionEnemigo--;
                TurnoEnemigo = false;
            }
            else
            {
                RespuestaFalsa = true;
                PuntuacionJugador--;
                TurnoEnemigo = true;
            }
        }
        else if (!TurnoEnemigo)
        {
            if (Option == Respuesta)
            {
                RespuestaFalsa = false;
                PuntuacionJugador--;
                TurnoEnemigo = true;
            }
            else
            {
                RespuestaFalsa = true;
                PuntuacionEnemigo--;
                TurnoEnemigo = false;
            }
        }
        ChangeColor(RespuestaFalsa);
        StartCoroutine (Waiter());
        WinGame();
        DefaultColor();
        ChangeText();
        Selected = false;
    }

    private void WinGame()
    {
        switch (PuntuacionJugador)
        {
            case 1:
                ScorePlayer.GetComponentInChildren<UnityEngine.UI.Text>().text = "Jugador = 1";
                break;
            case 2:
                ScorePlayer.GetComponentInChildren<UnityEngine.UI.Text>().text = "Jugador = 2";
                break;
            case 3:
                ScorePlayer.GetComponentInChildren<UnityEngine.UI.Text>().text = "Jugador = 3";
                break;
        }
        switch (PuntuacionEnemigo)
        {
            case 1:
                ScoreEnemy.GetComponentInChildren<UnityEngine.UI.Text>().text = "Enemigo = 1";
                break;
            case 2:
                ScoreEnemy.GetComponentInChildren<UnityEngine.UI.Text>().text = "Enemigo = 2";
                break;
            case 3:
                ScoreEnemy.GetComponentInChildren<UnityEngine.UI.Text>().text = "Enemigo = 3";
                break;
        }
        if(PuntuacionEnemigo == 0 || PuntuacionJugador == 0)
        {
            SceneManager.LoadScene("FinalPartida", LoadSceneMode.Single);
        }
    }

    private void ChangeColor(bool RespuestaEquivocada)
    {
        if (!RespuestaEquivocada)
        {
            switch (Respuesta)
            {
                case 1:
                    Option1.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case 2:
                    Option2.gameObject.GetComponent<Image>().color = Color.green;
                    break;

            }

        }
        else
        {
            switch (Respuesta)
            {
                case 1:
                    Option1.gameObject.GetComponent<Image>().color = Color.red;
                    break;
                case 2:
                    Option2.gameObject.GetComponent<Image>().color = Color.red;
                    break;
            }
        }

    }

    private void DefaultColor()
    {   
        Option1.gameObject.GetComponent<Image>().color = Color.white;
        Option2.gameObject.GetComponent<Image>().color = Color.white;
    }

    void Update()
    {
        if (PrimerTurno)
        {
            AsignarPrimerTurno();
            WinGame();
            ChangeText();
            PrimerTurno = false;
        }
        if (Selected)
        {
            Comprobacion(); 
        }
    }
}
