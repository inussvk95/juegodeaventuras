﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

    public static bool GameIsPause = false;
    public static bool Final = false;
    public GameObject pauseMenuUI;
    public GameObject Game;



    void Resume()
    {
        pauseMenuUI.SetActive(false);
        Game.SetActive(true);
        GameIsPause = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Game.SetActive(false);
        GameIsPause = true;
    }

    public void RestartGame()
    {
        Resume();
        Insultos.Option = 0;
        Insultos.Selected = false;
        Insultos.PuntuacionEnemigo = 3;
        Insultos.PuntuacionJugador = 3;
        Insultos.PrimerTurno = true;
    }
    void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPause)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }


	}
}
